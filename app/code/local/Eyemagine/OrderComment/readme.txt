/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * OrderComment
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_OrderComment
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

-------------------------------------------------------------------------------
DESCRIPTION:
-------------------------------------------------------------------------------

Allows customers to add a comment to their order at checkout

Module Files:

  - app/etc/modules/Eyemagine_OrderComment.xml
  - app/code/local/Eyemagine/OrderComment/*


-------------------------------------------------------------------------------
COMPATIBILITY:
-------------------------------------------------------------------------------

  - Tested only on Magento Enterprise Edition 1.12.0.2
  

-------------------------------------------------------------------------------
RELEASE NOTES:
-------------------------------------------------------------------------------
    
v1.0.0: March 28, 2013
  - Initial release