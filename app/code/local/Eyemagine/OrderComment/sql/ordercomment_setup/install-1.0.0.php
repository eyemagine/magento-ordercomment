<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * OrderComment
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_OrderComment
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

$this->startSetup();

$newAttr = array(
  'entity_type_id'  => 11,         // sales/order
  'attribute_code'  => 'order_comment',
  'backend_type'    => 'text',     // MySQL-DataType
  'frontend_input'  => 'textarea', // Type of the HTML-Form-Field
  'is_global'       => '1',
  'is_visible'      => '1',
  'is_required'     => '0',
  'is_user_defined' => '0',
  'frontend_label'  => 'Customer Comment',
);

$attribute = new Mage_Eav_Model_Entity_Attribute();

$attribute->loadByCode($newAttr['entity_type_id'], $newAttr['attribute_code'])
          ->setStoreId(0)
          ->addData($newAttr);
          
$attribute->save();

$this->endSetup();