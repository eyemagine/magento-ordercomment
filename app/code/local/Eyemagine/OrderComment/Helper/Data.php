<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * OrderComment
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_OrderComment
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class Eyemagine_OrderComment_Helper_Data
    extends Mage_Core_Helper_Abstract
{
    
}